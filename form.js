

const valider = () =>{
    const nomProduit = document.getElementById('nomProduit').value;
    const idProduit = document.getElementById("idProduit").value;

    const messageRecu = document.getElementById("messageRecu");

    console.log('nomProduit ',nomProduit);
    console.log('idProduit ',idProduit);

    console.log('idProduit ',idProduit.length);

    let validation = "ok";
    let message = "Le formulaire a été envoyé avec succès";
    // Tous les champs sont obligatoires
    if( !nomProduit || !idProduit ){
        validation = "ko";
        message = "Tous les champs sont obligatoires";
        console.log('error empty');
    }
    // NomProduit

    // Doit contenir minimum 3 caractères
    // Doit contenir maximum 25 caractères
    if( nomProduit.length<3 || nomProduit.length>25 ){
        validation = "ko";
        message = "nomProduit : Doit contenir entre 3 et 25 caractères";
        console.log('error lenght');
    }

    // Doit être entièrement saisie en minuscule
    if (nomProduit.match(/[A-Z]/, 'g')) {
        validation = "ko";
        message = "nomProduit : Doit être entièrement saisie en minuscule";
        console.log('error Upper');
    }
    console.log('validation ',validation);

    // idProduit

    // Doit correspondre à une série de caractères bien définis : 
    // 2 chiffres, 1 tiret, 4 lettres, 1 tiret, 1 chiffre, 1 tiret et 1 lettre (Ex : 12-ABCD-3-E, 23-DFKG-5-J )
    // 2 chiffres
    console.log('idProduit.substr(0, 2) ',idProduit.substr(0, 2));
    if(!idProduit.substr(0, 2).match(/[0-9]/, 'g')){
        validation = "ko";
        console.log('idProduit.substr(0, 2) ',idProduit.substr(0, 2));
        console.log('error number');
        message = "Doit correspondre à une série de caractères bien définis : Ex : 12-ABCD-3-E, 23-DFKG-5-J ";
    }
    //1 tiret
    if(!idProduit.substr(2, 1) == '-'){
        validation = "ko";
        console.log('idProduit.substr(2, 1) ',idProduit.substr(2, 1));
        console.log('error tiret');
        message = "Doit correspondre à une série de caractères bien définis : Ex : 12-ABCD-3-E, 23-DFKG-5-J ";
    }
    //4 lettres
    if(!idProduit.substr(3, 4).match(/[A-Z]/, 'g')){
        validation = "ko";
        console.log('idProduit.substr(3, 4) ',idProduit.substr(3, 4));
        console.log('error lettre');
        message = "Doit correspondre à une série de caractères bien définis : Ex : 12-ABCD-3-E, 23-DFKG-5-J ";
    }
    // 1 chiffre
    if(!idProduit.substr(8, 1).match(/[0-9]/, 'g')){
        validation = "ko";
        console.log('idProduit.substr(7, 1) ',idProduit.substr(7, 1));
        console.log('error number');
        message = "Doit correspondre à une série de caractères bien définis : Ex : 12-ABCD-3-E, 23-DFKG-5-J ";
        
    }
    //1 tiret
    if(!idProduit.substr(9, 1) == '-'){
        validation = "ko";
        console.log('idProduit.substr(8, 1) ',idProduit.substr(8, 1));
        console.log('error tiret');
        message = "Doit correspondre à une série de caractères bien définis : Ex : 12-ABCD-3-E, 23-DFKG-5-J ";
    }
    //1 lettre
    if(!idProduit.substr(10, 1).match(/[A-Z]/, 'g')){
        validation = "ko";
        console.log('idProduit.substr(9, 1) ',idProduit.substr(9, 1));
        console.log('error lettre');
        message = "Doit correspondre à une série de caractères bien définis : Ex : 12-ABCD-3-E, 23-DFKG-5-J ";
    }

    messageRecu.innerHTML="<p>"+message+"</p>";
    if(validation == "ok"){
        messageRecu.style.color = 'green';
    }
    else{
        messageRecu.style.color = 'red';
    }
}

const reset = () =>{
    const nomProduit = document.getElementById('nomProduit');
    const idProduit = document.getElementById("idProduit");
    nomProduit.value= " ";
    idProduit.value= " ";
}